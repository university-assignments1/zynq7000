/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_mmu.h"

#include "defs.h"
#include <sleep.h>

#include "led_ip.h"

#include "xscutimer.h"
#include "xscugic.h"

XScuTimer Timer;
XScuGic IntcInstance;

#define CPU1_TIME 			XPAR_PS7_CORTEXA9_1_CPU_CLK_FREQ_HZ / 2
#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID
#define TIMER_IRPT_INTR		XPAR_SCUTIMER_INTR

static void TimerIntrHandler(void *CallBackRef);

int main()
{
    init_platform();

    xil_printf("CPU1 started\r\n");

    int status;

    // Disable cache for shared variables
	Xil_SetTlbAttributes(S_BASEADDR, TYPE_SHARE);
	Xil_SetTlbAttributes(C_BASEADDR, TYPE_SHARE);
	Xil_SetTlbAttributes(SEM0_BASEADDR, TYPE_SHARE);
	Xil_SetTlbAttributes(SEM1_BASEADDR, TYPE_SHARE);
	Xil_SetTlbAttributes(TURN_BASEADDR, TYPE_SHARE);

	// Setup interrupt Handler
	XScuGic_Config *IntcConfig;

	// Initialize the GIC (Generic Interrupt Controller)
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	status = XScuGic_CfgInitialize(&IntcInstance, IntcConfig,
			IntcConfig->CpuBaseAddress);

	if (status != XST_SUCCESS) {
		xil_printf("GIC initialization failed");
		return XST_FAILURE;
	}

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
			(Xil_ExceptionHandler)XScuGic_InterruptHandler,
			&IntcInstance);

	status = XScuGic_Connect(&IntcInstance, TIMER_IRPT_INTR,
			(Xil_ExceptionHandler)TimerIntrHandler,
			(void *)&Timer);

	if (status != XST_SUCCESS) {
		xil_printf("XScu Connection failed (error %d)", status);
		return status;
	}

	XScuGic_Enable(&IntcInstance, TIMER_IRPT_INTR);

	// Setup ARM timer
	XScuTimer_Config *ConfigPtr;

	// Initialize the timer
	ConfigPtr = XScuTimer_LookupConfig (XPAR_PS7_SCUTIMER_0_DEVICE_ID);
	status = XScuTimer_CfgInitialize(&Timer, ConfigPtr, ConfigPtr->BaseAddr);

	if(status != XST_SUCCESS){
	  xil_printf("Timer init() failed\r\n");
	  return XST_FAILURE;
	}

	XScuTimer_EnableInterrupt(&Timer);

	Xil_ExceptionEnable();

	// Load timer with CPU0_TIME
	XScuTimer_LoadTimer(&Timer, CPU1_TIME);

	// Set AutoLoad mode
	XScuTimer_EnableAutoReload(&Timer);

	// Start the timer
	XScuTimer_Start(&Timer);

	while(1) {

		// #2
		LED_IP_mWriteReg(XPAR_LED_IP_0_S_AXI_BASEADDR, LED_IP_S_AXI_SLV_REG0_OFFSET, S);
	}

	cleanup_platform();
	return 0;
}

static void TimerIntrHandler(void *CallBackRef)
{
	SEM1=1;
	TURN=0;

	// Wait for CPU1 to clear the semaphore
	while(SEM0 == 1 && TURN == 0);

	// clear status bit
	XScuTimer_ClearInterruptStatus(&Timer);
	xil_printf("CPU1: %d\n\r", --C);

	SEM1=0;
}
