/*
 * defs.h
 *
 *  Created on: 15 ��� 2019
 *      Author: Ageor
 */

#ifndef XIL_MMU_H
#include "ps7_cortexa9_0/include/xil_mmu.h"
#endif

#ifndef DEFS_H_
#define DEFS_H_

// BASE ADDRESSES ENUM
#define BASE 0xFFFF0000
#define STEP 4
#define NEXT_ENUM_MEMBER(NAME,PREVIOUS) \
    NAME = PREVIOUS + STEP

enum BASEADDRS {
	S_BASEADDR = BASE,
	NEXT_ENUM_MEMBER(C_BASEADDR, S_BASEADDR),

	// Shared variables used in Peterson's algorithm
	NEXT_ENUM_MEMBER(SEM0_BASEADDR, C_BASEADDR),
	NEXT_ENUM_MEMBER(SEM1_BASEADDR, SEM0_BASEADDR),
	NEXT_ENUM_MEMBER(TURN_BASEADDR, SEM1_BASEADDR)
};

#define TYPE_SHARE (NORM_NONCACHE | SHAREABLE)

// define S shared variable
#define S (*(volatile unsigned int *) (S_BASEADDR))
// define COUNTER shared variable
#define C (*(volatile unsigned int *) (C_BASEADDR))
// define CPU0 semaphore
#define SEM0 (*(volatile unsigned int *) (SEM0_BASEADDR))
// define a semaphore
#define SEM1 (*(volatile unsigned int *) (SEM1_BASEADDR))
// define turn variable for Peterson's solution
#define TURN (*(volatile unsigned int *) (TURN_BASEADDR))

#endif /* DEFS_H_ */
