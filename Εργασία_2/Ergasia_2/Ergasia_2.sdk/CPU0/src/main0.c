/*
 * main0.c
 *
 *  Created on: 13 ��� 2020
 *      Author: Ageor
 */

#include "xil_printf.h"
#include "xil_mmu.h"
#include "xtime_l.h"

#include "utils.h"
#include "shared.h"

void multicore();
void singlecore();

void print_defs()
{
	xil_printf("IN_TABLE: %x\r\n", IN_TABLE);
	xil_printf("OUT_TABLE: %x\r\n", OUT_TABLE);
	xil_printf("FLAGS: %x\r\n", FLAGS_BASE);
}

int main()
{
	print_hello(0);
	print_defs();

	// Setup OCM for shared variables
	Xil_SetTlbAttributes(IN_TABLE, TYPE_SHARE_TLB);
	Xil_SetTlbAttributes(OUT_TABLE, TYPE_SHARE_TLB);
	Xil_SetTlbAttributes(FLAGS_BASE, TYPE_SHARE_TLB);

	XTime tStart, tEnd;

	// Get the average time for multicore.
	float avg=0;
	for (int i = 0; i < 10; i++) {
		XTime_GetTime(&tStart);
		multicore();
		XTime_GetTime(&tEnd);
		avg += (tEnd - tStart) / (COUNTS_PER_SECOND / 1000);
	}
	avg /= 10.0;
	xil_printf("Average time for multicore %d ms\r\n", (int)avg);

	avg=0;
	for (int i = 0; i < 10; i++) {
		XTime_GetTime(&tStart);
		singlecore();
		XTime_GetTime(&tEnd);
		avg += (tEnd - tStart) / (COUNTS_PER_SECOND / 1000);
	}
	avg /= 10.0;
	xil_printf("Average time for singlecore %d ms\r\n", (int)avg);

	for(;;);

	return 0;
}

void singlecore()
{
	// 1. Generate 128*128 table
	fill_table(IN_TABLE);
	xil_printf("Table generation completed\r\n");

	// 2. Convolution
	convolution(in_table, out_table, 0);
	convolution(in_table, out_table, 1);

	xil_printf("Convolution completed\r\n");

	// 3. Quantization
	quantization(out_table, 0);
	quantization(out_table, 1);

	xil_printf("Quantization completed\r\n");

	// 4. Histogram
	int bins[8];
	histogram(out_table, bins, 0);
	histogram(out_table, bins, 1);

	xil_printf("Histogram completed\r\n");

	for (int i = 0; i < 8; i++) {
		xil_printf("%d ", bins[i]);
	}

	xil_printf("\r\n");
}

void multicore()
{
	FLAGS |= CPU0_READY_F;
	while(wait_for_flag(FLAGS, CPU1_READY_F));

	// 1. Generate 128*128 table
	fill_table(IN_TABLE);
	xil_printf("Table generation completed\r\n");
	FLAGS |= TABLE_GEN_F;

	// 2. Convolution
	convolution(in_table, out_table, 0);
	FLAGS |= CONV0_F;

	// Wait for CPU1
	while(wait_for_flag(FLAGS, CONV1_F));

	xil_printf("Convolution completed\r\n");

	// 3. Quantization
	quantization(out_table, 1);
	FLAGS |= QUAN0_F;

	while(wait_for_flag(FLAGS, QUAN1_F));

	xil_printf("Quantization completed\r\n");

	// 4. Histogram
	histogram(out_table, bins0, 0);
	FLAGS |= HIST0_F;

	while(wait_for_flag(FLAGS, HIST1_F));
	FLAGS = 0;

	// Merge the bins from CPU0 and CPU1.
	int bins[8];
	for (int i = 0; i < 8; i ++)
		bins[i] = bins0[i] + bins1[i];

	xil_printf("Histogram completed\r\n");

	for (int i = 0; i < 8; i++) {
		xil_printf("%d ", bins[i]);
	}

	xil_printf("\r\n");
}
