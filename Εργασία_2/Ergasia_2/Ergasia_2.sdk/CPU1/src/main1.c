/*
 * main1.c
 *
 *  Created on: 13 ��� 2020
 *      Author: Ageor
 */

#include "xil_printf.h"
#include "xil_mmu.h"

#include "utils.h"
#include "shared.h"

int main()
{
	start:

	FLAGS |= CPU1_READY_F;
	while(wait_for_flag(FLAGS, CPU0_READY_F));

	print_hello(1);

	// Setup OCM for shared variables
	Xil_SetTlbAttributes(IN_TABLE, TYPE_SHARE_TLB);
	Xil_SetTlbAttributes(OUT_TABLE, TYPE_SHARE_TLB);
	Xil_SetTlbAttributes(FLAGS_BASE, TYPE_SHARE_TLB);

	// 1. Wait for table generation to complete.
	while(wait_for_flag(FLAGS, TABLE_GEN_F));

	// 2. Convolution
	convolution(in_table, out_table, 1);
	FLAGS |= CONV1_F;

	// Wait for CPU0;
	while(wait_for_flag(FLAGS, CONV0_F));

	// 3. Quantization
	quantization(out_table, 0);
	FLAGS |= QUAN1_F;

	while(wait_for_flag(FLAGS, QUAN0_F));

	// 4. Histogram
	histogram(out_table, bins1, 1);
	FLAGS |= HIST1_F;

	while(wait_for_flag(FLAGS, HIST0_F));
	FLAGS = 0;

	goto start;

	for(;;);

	return 0;
}
