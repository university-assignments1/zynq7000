/*
 * shared.h
 *
 *  Created on: 13 ��� 2020
 *      Author: Ageor
 */

#ifndef SRC_SHARED_H_
#define SRC_SHARED_H_

#include "xil_mmu.h"

#define SHARED_TYPE(V)	(*(volatile unsigned int *) (V))
#define TYPE_SHARE_TLB	(NORM_NONCACHE | SHAREABLE)

// BASE ADDRESSES ENUM
#define BASE		0xFFFF0000 // OCM is mapped high
#define BASE_END	BASE
#define NEXT_ENUM_MEMBER(NAME,PREVIOUS,STEP) \
    NAME = PREVIOUS##_END,\
	NAME##_END = PREVIOUS##_END + STEP

enum BASEADDRS {
	NEXT_ENUM_MEMBER(IN_TABLE, BASE, 128*128),
	NEXT_ENUM_MEMBER(OUT_TABLE, IN_TABLE, 128*128),
	NEXT_ENUM_MEMBER(FLAGS_BASE, OUT_TABLE, 4),
	NEXT_ENUM_MEMBER(BINS0_BASE, FLAGS_BASE, 8*4),
	NEXT_ENUM_MEMBER(BINS1_BASE, BINS0_BASE, 8*4)
};

extern volatile uint8_t (* const in_table)[128] = (uint8_t *)IN_TABLE;
extern volatile uint8_t (* const out_table)[128] = (uint8_t *)OUT_TABLE;
extern volatile int * const bins0 = (int *)BINS0_BASE;
extern volatile int * const bins1 = (int *)BINS1_BASE;

#define FLAGS	SHARED_TYPE(FLAGS_BASE)

// FLAGS BIT MASKS
#define TABLE_GEN_F		(1 << 0) // bit 0 is for step 1 (table generation)
#define CONV0_F			(1 << 1) // bit 1 is for step 2 CPU0 (convolution 0)
#define CONV1_F			(1 << 2) // bit 2 is for step 2 CPU1 (convolution 1)
#define QUAN0_F			(1 << 3) // bit 3 is for step 3 CPU0 (quantization 0)
#define QUAN1_F			(1 << 4) // bit 4 is for step 3 CPU1 (quantization 1)
#define HIST0_F			(1 << 5) // bit 5 is for step 4 CPU0 (histogram 0)
#define HIST1_F			(1 << 6) // bit 6 is for step 4 CPU1 (histogram 1)
#define CPU0_READY_F	(1 << 7) // bit 7 is for CPU0 ready flag
#define CPU1_READY_F	(1 << 8) // bit 8 is for CPU1 ready flag

#endif /* SRC_SHARED_H_ */
