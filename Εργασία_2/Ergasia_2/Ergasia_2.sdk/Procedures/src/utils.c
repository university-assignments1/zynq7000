/*
 * convolution.c
 *
 *  Created on: 12 ��� 2020
 *      Author: Ageor
 */

#include "utils.h"

#include "xil_printf.h"
#include "xil_io.h"
#include "xil_types.h"


void print_hello(int cpuid)
{
	xil_printf("Hello from cpu%d\r\n", cpuid);
}

void fill_table(uintptr_t table_address)
{
	u32 value;
	for (u32 i = 0; i < 128; i++) {
		value = i | (i << 8) | (i << 16) | (i << 24);
		for (u32 j = 0; j < 128; j+=4) {
			Xil_Out32(table_address + i*128 + j, value);
		}
	}
}

int convolution(volatile uint8_t (*in_table)[128],volatile uint8_t (*out_table)[128], int section)
{
	int matrix[3][3] = {{-1, -1, -1},
						{-1, 9, -1},
						{-1, -1, -1}};

	int tmp;

	if (section == 0) {

		// Convolution in internal table (exclude borders).
		for (int i = 1; i < 64; i++) {
			for (int j = 1; j < 127; j++) {
				tmp = 0;
				for (int k = 0; k < 3; k++) {
					for (int l = 0; l < 3; l++) {
						tmp += in_table[i + k - 1][j + l - 1] * matrix[k][l];
					}
				}
				if (tmp > 255) out_table[i][j] = 255;
				else if (tmp < 0) out_table[i][j] = 0;
				else out_table[i][j] = (uint8_t) tmp;
			}
		}

		// Convolution in left up corner.
		tmp = 	in_table[0][0] * matrix[1][1] +
				in_table[0][1] * matrix[1][2] +
				in_table[1][0] * matrix[2][1] +
				in_table[1][1] * matrix[2][2];

		if (tmp > 255) out_table[0][0] = 255;
		else if (tmp < 0) out_table[0][0] = 0;
		else out_table[0][0] = (uint8_t) tmp;

		// Convolution in right up corner.
		tmp = 	in_table[0][127] * matrix[1][1] +
				in_table[0][126] * matrix[1][0] +
				in_table[1][127] * matrix[2][1] +
				in_table[0][126] * matrix[2][0];

		if (tmp > 255) out_table[0][127] = 255;
		else if (tmp < 0) out_table[0][127] = 0;
		else out_table[0][127] = (uint8_t) tmp;

		// Convolution in top horizontal line.
		for (int j = 1; j < 127; j++) {
			tmp = 	in_table[0][j] * matrix[1][1] +
					in_table[0][j-1] * matrix[1][0] +
					in_table[0][j+1] * matrix[1][2] +
					in_table[1][j] * matrix[2][1] +
					in_table[1][j-1] * matrix[2][0] +
					in_table[1][j+1] * matrix[2][2];

			if (tmp > 255) out_table[0][j] = 255;
			else if (tmp < 0) out_table[0][j] = 0;
			else out_table[0][j] = (uint8_t) tmp;
		}

		// Convolution in left vertical line.
		for (int i = 1; i < 64; i++) {
			tmp = 	in_table[i][0] * matrix[1][1] +
					in_table[i-1][0] * matrix[0][1] +
					in_table[i+1][0] * matrix[2][1] +
					in_table[i][1] * matrix[1][2] +
					in_table[i-1][1] * matrix[0][2] +
					in_table[i+1][1] * matrix[2][2];

			if (tmp > 255) out_table[i][0] = 255;
			else if (tmp < 0) out_table[i][0] = 0;
			else out_table[i][0] = (uint8_t) tmp;
		}

		// Convolution in right vertical line.
		for (int i = 1; i < 64; i++) {
			tmp = 	in_table[i][127] * matrix[1][1] +
					in_table[i-1][127] * matrix[0][1] +
					in_table[i+1][127] * matrix[2][1] +
					in_table[i][126] * matrix[1][0] +
					in_table[i-1][126] * matrix[0][0] +
					in_table[i+1][126] * matrix[2][0];

			if (tmp > 255) out_table[i][127] = 255;
			else if (tmp < 0) out_table[i][127] = 0;
			else out_table[i][127] = (uint8_t) tmp;
		}

	} else if (section == 1) {

		// Convolution in internal table (exclude borders).
		for (int i = 64; i < 127; i++) {
			for (int j = 1; j < 127; j++) {
				tmp = 0;
				for (int k = 0; k < 3; k++) {
					for (int l = 0; l < 3; l++) {
						tmp += in_table[i + k - 1][j + l - 1] * matrix[k][l];
					}
				}
				if (tmp > 255) out_table[i][j] = 255;
				else if (tmp < 0) out_table[i][j] = 0;
				else out_table[i][j] = (uint8_t) tmp;
			}
		}

		// Convolution in left down corner.
		tmp = 	in_table[127][0] * matrix[1][1] +
				in_table[127][1] * matrix[1][2] +
				in_table[126][0] * matrix[0][1] +
				in_table[126][1] * matrix[0][2];

		if (tmp > 255) out_table[0][0] = 255;
		else if (tmp < 0) out_table[0][0] = 0;
		else out_table[0][0] = (uint8_t) tmp;

		// Convolution in right down corner.
		tmp = 	in_table[127][127] * matrix[1][1] +
				in_table[127][126] * matrix[1][0] +
				in_table[126][127] * matrix[0][1] +
				in_table[126][126] * matrix[0][0];

		if (tmp > 255) out_table[0][127] = 255;
		else if (tmp < 0) out_table[0][127] = 0;
		else out_table[0][127] = (uint8_t) tmp;

		// Convolution in bottom horizontal line.
		for (int j = 1; j < 127; j++) {
			tmp = 	in_table[127][j] * matrix[1][1] +
					in_table[127][j-1] * matrix[1][0] +
					in_table[127][j+1] * matrix[1][2] +
					in_table[126][j] * matrix[0][1] +
					in_table[126][j-1] * matrix[0][0] +
					in_table[126][j+1] * matrix[0][2];

			if (tmp > 255) out_table[0][j] = 255;
			else if (tmp < 0) out_table[0][j] = 0;
			else out_table[0][j] = (uint8_t) tmp;
		}

		// Convolution in left vertical line.
		for (int i = 64; i < 127; i++) {
			tmp = 	in_table[i][0] * matrix[1][1] +
					in_table[i-1][0] * matrix[0][1] +
					in_table[i+1][0] * matrix[2][1] +
					in_table[i][1] * matrix[1][2] +
					in_table[i-1][1] * matrix[0][2] +
					in_table[i+1][1] * matrix[2][2];

			if (tmp > 255) out_table[i][0] = 255;
			else if (tmp < 0) out_table[i][0] = 0;
			else out_table[i][0] = (uint8_t) tmp;
		}

		// Convolution in right vertical line.
		for (int i = 64; i < 127; i++) {
			tmp = 	in_table[i][127] * matrix[1][1] +
					in_table[i-1][127] * matrix[0][1] +
					in_table[i+1][127] * matrix[2][1] +
					in_table[i][126] * matrix[1][0] +
					in_table[i-1][126] * matrix[0][0] +
					in_table[i+1][126] * matrix[2][0];

			if (tmp > 255) out_table[i][127] = 255;
			else if (tmp < 0) out_table[i][127] = 0;
			else out_table[i][127] = (uint8_t) tmp;
		}

	} else {
		return -1;
	}

	return 0;
}

int quantization(volatile uint8_t (*table)[128], int section)
{
	if (section > 1)
		return -1;

	int start = (section == 0) ? 0 : 64;

	for (int i = start; i < 128 - 64 + start; i++) {
		for (int j = 0; j < 128; j++) {
			table[i][j] /= 32;
		}
	}

	return 0;
}

int histogram(volatile uint8_t (*table)[128], volatile int *bins, int section)
{
	if (section > 1)
		return -1;

	for (int i = 0; i < 8; i++)
		bins[i] = 0;

	int start = (section == 0) ? 0 : 64;

	for (int i = start; i < 128 - 64 + start; i ++) {
		for (int j = 0; j < 128; j++) {
			bins[table[i][j]]++;
		}
	}

	return 0;
}

int wait_for_flag(u32 flag_register, u32 field)
{
	return !(flag_register & field);
}
