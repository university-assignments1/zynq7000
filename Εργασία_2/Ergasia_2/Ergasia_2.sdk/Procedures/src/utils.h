/*
 * convolution.h
 *
 *  Created on: 12 ��� 2020
 *      Author: Ageor
 */

#ifndef INCLUDE_CONVOLUTION_H_
#define INCLUDE_CONVOLUTION_H_

#include "xil_types.h"

void print_hello(int cpuid);
void fill_table(uintptr_t table_address);

int wait_for_flag(u32 flag_register, u32 field);

int convolution(volatile uint8_t (*in_table)[128],volatile uint8_t (*out_table)[128], int section);
int quantization(volatile uint8_t (*table)[128], int section);
int histogram(volatile uint8_t (*table)[128], volatile int *bins, int section);

#endif /* INCLUDE_CONVOLUTION_H_ */
